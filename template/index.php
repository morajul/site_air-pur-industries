<?php include("doctype.tpl"); ?>
<head>
	<title>Air Pur Industries, accueil</title>
	<?php include("meta.tpl"); ?>
</head>
<body id="banniere">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div class="contenu" id="contenu">
			<h1 id="titre">Contrôle et maintenance des appareils à pression</h1>
			<h2>Centre agréé pour l'épreuve et la requalification des appareils haute pression</h2>
			<p>Nous réalisons depuis 1990 les épreuves et la requalification de récipients de gaz comprimés à haute pression : bouteilles d'air respirable, bouteilles de plongée, bouteilles tampon, bouteilles de gaz divers et cuves de freinage pour camion.</p>
			<p>Nous vous proposons également :</p>
			<ul>
				<li>Le traitement de surface de vos bouteilles : microbillage intérieur, grenaillage, peinture extérieure</li>
				<li>La recharge de vos bouteilles en air respirable, gaz carbonique et oxygène</li>
				<li>L'entretien des extincteurs</li>
				<li>Les analyses d'air</li>
			</ul><br>
			<img id="logoiso" src="images/logo_veritas.png" alt="Certification ISO9001" title="Certification ISO9001 normes 2008">
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
