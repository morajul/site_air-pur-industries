<?php include("doctype.tpl"); ?>
<head>
	<title>Air Pur Industries, l'entreprise</title>
	<?php include("meta.tpl"); ?>
</head>
<body id="banniere">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div id="contenu">
			<img id="clientele" alt="Carte de France donnant une localisation générale de nos clients" src="images/carte_france.svg" height="283" width="273">
			<h2 id="client">Notre clientèle</h2>
			<p>Air Pur Industries, une PME avec un rayonnement national.</p>
			<p>Des clients dans toute la France :<br>
			Les Sapeurs Pompiers, Air Liquide, Draeger, Magasins et Clubs de plongée, EDF, CEA, Arcelor, All Chem, PSA, Michelin et bien d'autres nous font confiance.</p>
			<p><a class="remonte" href="#banniere">Haut de page</a></p>
			<h2 id="moyen">Des moyens adaptés au service de nos clients</h2>
			<ul>
				<li>Un bâtiment de 400m²</li>
				<li>Un banc d'épreuve et de test des appareils respiratoires</li>
				<li>Des installations de transvasement de gaz (gaz carbonique et oxygène)</li>
				<li>Une installation de chargement des bouteilles d'air respirable en haute pression</li>
				<li>Un compresseur basse pression</li>
				<li>Une cabine de peinture</li>
			</ul>
			<p>Pour faciliter la maintenance de vos appareils, nous assurons des circuits de ramassage et de livraison chez nos clients.<br>
			<a class="remonte" href="#banniere">Haut de page</a></p>
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
