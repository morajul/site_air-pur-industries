<?php include("doctype.tpl"); ?>
<head>
	<title>Air Pur Industries, Politique d'accessibilité</title>
	<?php include("meta.tpl"); ?>
</head>
<body id="banniere">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div id="contenu">
<h1>Aides à la navigation</h1>
			<h2>Raccourcis-claviers</h2>
			<ul id="accesskeys">
				<li><strong>S</strong> : Aller au contenu d'une page</li>
				<li><strong>0</strong> : Accéder aux règles d'accessibilité</li>
				<li><strong>1</strong> : Aller à l'accueil</li>
				<li><strong>9</strong> : Nous contacter par courriel</li>
			</ul>
			<h2>Mode d'emploi</h2>
			<p>Suivant votre navigateur, appuyer simultanément sur les touches indiquées :</p>
			<p><em><strong>Mozilla, Firefox, Netscape, K-meleon, Galeon</strong></em> : Touches Alt, Maj et une des touches ci-dessus (hors pavé numérique)</p>
			<p><em><strong>Internet Explorer sous Windows</strong></em> : Touches Alt, Maj et une des touches ci-dessus (hors pavé numérique) puis Entrée</p>
			<p><em><strong>Safari 1.2, Mozilla, Netscape sous Macintosh</strong></em> : Touches Ctrl, Maj et une des touches ci-dessus (hors pavé numérique)</p>
			<p><em><strong>Internet explorer sous Macintosh</strong></em> : Touches Ctrl, Maj et une des touches ci-dessus (hors pavé numérique) puis Entrée</p>
			<p><em><strong>Opera</strong></em> : Touches Echap, Maj et une des touches ci-dessus (hors pavé numérique)</p>
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
