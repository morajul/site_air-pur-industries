<?php include("doctype.tpl"); ?>
<head>
	<title>Air Pur Industries, Activités</title>
	<?php include("meta.tpl"); ?>
</head>
<body id="banniere">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div id="contenu">
			<h2 id="entretien">Le contrôle des appareils à pression, un besoin réglementaire dicté par le besoin de sécurité</h2>
			<p>Les appareils stockant ou utilisant des gaz sous pression contiennent une forte concentration d'énergie. Ils sont donc susceptibles d'être à l'origine d'un risque pour les personnes ou les installations.</p>
			<p>La vérification de la conformité des appareils à pression à des normes de résistance a donc pour but de <em><strong>protéger les personnes et les biens matériels</strong></em> .</p>
			<p>En France, les appareils à pression à gaz doivent faire l'objet d'un entretien régulier dont les modalités sont fixées par la loi.</p>
			<h2 id="bouteilles">L'activité bouteilles</h2><img id="tofbouteilles" alt="Exemples de bouteilles" src="images/bouteilles2.jpg" height="209" width="250">
			<p>Le contrôle et l'épreuve des bouteilles d'air respirable est notre activité principale.<br>
			En 2004, nous avons réalisé le traitement de 7500 bouteilles.</p>
			<p><a class="remonte" href="#banniere">Haut de page</a></p>
			<h2 id="respi">Contrôle des appareils respiratoires</h2><img id="ARI" alt="Système de contrôle des appareils respiratoires" src="images/testair2.jpg" height="213" width="250">
			<p>Dans l'optique d'améliorer toujours le service rendu à nos clients, nous réalisons depuis Janvier 2003 le contrôle des appareils respiratoires de marque Fenzy&nbsp;®</p>
			<p><a class="remonte" href="#banniere">Haut de page</a></p>
			<h2 id="extincteurs">Remplissage et contrôle des extincteurs</h2>
			<p>Nous effectuons également le remplissage et le contrôle des extincteurs de toutes marques.</p>
			<p><a class="remonte" href="#banniere">Haut de page</a></p>
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
