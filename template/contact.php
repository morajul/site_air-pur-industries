<?php include("doctype.tpl"); ?>
<head>
	<title>Air Pur Industries, Contacts</title>
	<?php include("meta.tpl"); ?>
	<link rel="stylesheet" href="leaflet.css">
	<script src="leaflet.js" type="text/javascript"></script>
	<script type="text/javascript" src="map.js"></script>
</head>
<body id="banniere" onload="InitialiserCarte() ;">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div id="contenu">
			<h2 id="contacter">Nous contacter</h2>
			<p>Vous pouvez nous contacter à l'adresse suivante :</p>
			<p id="adresse"><strong>Air Pur Industries</strong><br>
			10, rue Edouard Belin<br>
			25400 EXINCOURT<br>
			tel : +33 (0)3 81 95 52 24<br>
			fax : +33 (0)3 81 94 68 95<br>
			<a href="mailto:contact@airpur-industries.com" accesskey="9">contact@airpur-industries.com</a><br>
			<br>
			du lundi au vendredi de 8H à 12H et de 13H30 à 17H00.</p>
			<h2>Plan d'accès</h2>
			<div id="map" style="height: 600px"></div>
			<p id="itineraire">Télécharger <a href="fichiers/itineraire.pdf">l'itinéraire détaillé</a> au format PDF (200Ko, lisible avec <a href="http://www.adobe.fr/products/acrobat/readstep2.html">Adobe Reader</a>)</p>
			<p>Pour toutes remarques ou commentaires concernant le site proprement dit, envoyer vos messages à l'adresse suivante : <a href="mailto:webmaster@airpur-industries.com">webmaster@airpur-industries.com</a></p>
			<p>Ce site est valide <abbr title="HyperText Markup Language">html5</abbr> et <abbr title="Cascading Style Sheet">CSS3</abbr>. 
			Pour une consultation dans les meilleures conditions, nous vous conseillons d'utiliser un navigateur respectant les standards du web comme <a href="https://www.mozilla.org/fr/firefox">Mozilla&nbsp;Firefox</a>.</p>
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
