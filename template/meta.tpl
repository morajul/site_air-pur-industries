<meta charset="UTF-8">
	<meta name="description" content="Air Pur Industries, contrôle, inspection, épreuve, traitement des appareils à pression : bouteilles de plongées, compresseurs, appareils respiratoires isolants (ARI)">
	<meta name="keywords" content="contrôle, épreuve, bouteilles, bouteille de plongée, extincteurs, compresseurs, air respirable, ARI, appareil respiratoire">
	<meta name="Identifier-URL" content="https://www.airpur-industries.com/">
	<link rel="stylesheet" href="style.css">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="icon" type="image/png" href="images/favicon.png">
	<meta name="viewport" content="width=device-width">
