<?php include("doctype.tpl"); ?>
<head>
	<title>Erreur 404 : Page non trouvée</title>
	<?php include("meta.tpl"); ?>
</head>
<body id="banniere">
	<div class="conteneur">
<?php include("banniere.tpl"); ?>
<?php include("menu.tpl"); ?>
		<div id="contenu">
<h1>La page que vous cherchez à obtenir n'existe pas</h1>
			<p>Vous voyez cette page parce que l'adresse de la page que vous cherchez à obtenir est incorrecte.</p>
			<p>Commencez par vérifier la syntaxe dans la barre d'adresse.</p>
			<p>Si cette erreur est apparue à la suite d'une navigation à travers le menu de ce site, veuillez avertir le webmaster en écrivant à <a href="mailto:webmaster@airpur-industries.com">webmaster@airpur-industries.com</a>.</p>
			<p>Si cette erreur est apparue à la suite de l'utilisation d'un lien depuis un autre site, veuillez avertir le webmaster du site initial.</p>
<?php include("pied.tpl"); ?>
<?php include("finpage.tpl"); ?>
