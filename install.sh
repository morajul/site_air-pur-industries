#! /bin/bash
unset NOM
unset CHEMIN
CHEMIN="/home/jules/Airpur/site_dev"

pushd $CHEMIN

	if [ -d "build" ];
		then
		rm -rf build/*
		else
		mkdir build
	fi


	pushd template
		for i in *.php ;do
			NOM="${i%.*}"
			php $i > ../build/$NOM.htm
		done
	popd

	yuicompressor -o build/style.css css/style.css
	yuicompressor -o build/leaflet.css css/leaflet.css
	yuicompressor -o build/map.js js/map.js
	cp -a js/leaflet.js build/leaflet.js
	cp -a images build/images
	cp -a fonts build/fonts
	cp -a fichiers build/fichiers
	cp -a .htaccess build/.htaccess
	cp -a robots.txt build/robots.txt

	pushd build
		ln -s images/favicon.ico favicon.ico
	popd

popd
