function InitialiserCarte() {
	// générer la carte dans l'id map aux coordonnées GPS de airpur et
	// au niveau de zoom 11
    var map = L.map('map').setView([47.5417, 6.8371], 11);

    // create the tile layer with correct attribution
    var tuileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    
    
    // message d'attribution des droits de la carte
    var attrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    
    
    var osm = L.tileLayer(tuileUrl, {
        minZoom: 3, 
        maxZoom: 19,
        attribution: attrib
    });

    osm.addTo(map);
    // mettre un marqueur aux coordonnées GPS
    var marker = L.marker([47.49650, 6.82390]).addTo(map);
    // mettre un message sur le marqueur
    marker.bindPopup('<p style="text-align: center; font-size: 120%;"><strong>Air Pur Industries</strong></p><p>Latitude : 47.49650°<br />Longitude : 6.82390°</p>');
}
